function createNewUser() {
  let firstName = prompt("Enter your first name:");
  let lastName = prompt("Enter your last name:");

  const newUser = {
   getLogin: function() {
  return firstName.charAt(0).toLowerCase() + lastName.toLowerCase();
},
    setFirstName: function(newFirstName) {
      firstName = newFirstName;
    },
    setLastName: function(newLastName) {
      lastName = newLastName;
    }
  };

  Object.defineProperty(newUser, "firstName", {
    get: function() {
      return firstName;
    },
    enumerable: true
  });

  Object.defineProperty(newUser, "lastName", {
    get: function() {
      return lastName;
    },
    enumerable: true
  });

  return newUser;
}

const user = createNewUser();
console.log(user.getLogin()); 

user.setFirstName("Ivan");
user.setLastName("Kravchenko");
console.log(user.getLogin()); 

user.firstName = "Антон"; //  Now it doesn't work 
console.log(user.getLogin()); 